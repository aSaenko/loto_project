<?php
//error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);  // проверка на на будущие проблемы в коде
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" integrity="sha512-NmLkDIU1C/C88wi324HBc+S2kLhi08PN5GDeUVVVC/BVt/9Izdsc9SVeVfA1UZbY3sHUlDSyRXhCzHfr6hmPPw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="style.css">
    <title>Loto</title>
</head>
<body>

<?php
define("MAX_NUM" , 52);     // максимальное число
define("NUMBER_INPUT" , 6); // количество полей для ввода
$dataFile = "SuperLoto_Results__918-2168.csv";

$arrayEntered = $_POST;
$arrayEntered = array_slice($arrayEntered, 0 , -1); // чистый ввод пользователя после обрезки , обрезает сабмит

$res = test_input($arrayEntered);    // массив после проверки на ошибки
$countNumbers = []; // посчитаные числа
$parseArray = []; // конечный файл после парсинга
?>


<div class="container">

    <?php if (!empty($countNumbers) || !checkingErrors($res)):?>
        <h1 class="title-text">Введите число от 1 до <?=MAX_NUM?></h1>
    <?php endif; ?>

    <div class="wrapper-content">

        <?php
        // вывод инпутов
        if (!empty($countNumbers) || !checkingErrors($res)){
            printForm(prepareFormData(NUMBER_INPUT,$res,$arrayEntered));
        }

        // кнопка повторного ввода
        if (checkingErrors($res)){
            btnSubmitAgain();
        }

        // вывод результат ввода
        if (isset($_POST['submit'])){
            if (checkingErrors($res)){
                createResultInput($arrayEntered);
            }
        }

        //вывод таблици результатов
        if(file_exists( $dataFile ) ){  // если файл есть
            if(checkingErrors($res)){
                $parseArray = parseFile($dataFile,$parseArray);      // парсит файл
                $countNumbers = countWins($parseArray,$countNumbers,$arrayEntered); // считает
                createResult($countNumbers); // выводит
            }
        }
        ?>

    </div>
</div>

</body>
</html>


<?php
function printForm ($array){ ?>
    <form action="" method="post" >
        <div class="form">

            <?php foreach ($array as $value):?>
                <div class='wrapper-loto-number'>
                    <input name='<?php echo $value["name"];?>' class = 'loto-number' value='<?php echo $value["value"]; ?>'>

                    <?php if (!empty($value["error"])):?>
                        <span class='error'> <?php echo $value["error"];?></span>
                    <?php endif;?>

                </div>
            <?php endforeach; ?>

        </div>
        <div class="sub">
            <input class="btn-loto-submit" type="submit" name="submit" value="Отправить">
        </div>
    </form>
    <?php
}

function btnSubmitAgain(){ ?>
    <form action="" method="post" >
        <div class="sub">
            <input class="btn-loto-submit" type="submit" name="submitAgain" value="Ввести еще раз">
        </div>
    </form>

    <?php
}

function createResultInput($data){ ?>
    <div class="result-input">
        <?php foreach ($data as $value) :?>
            <p class='is-number'><?=$value?></p>
        <?php endforeach; ?>
    </div>
<?php
}

function createResult ($countNumbers){ ?>
    <section class="win-table">
        <h1 class='result-title'>Таблица результатов</h1>
        <div class='header-results'>
            <div class='wrapper-result'>
                <div class='how-title'>Номеров угадано</div>
                <p class='how-title'>Выигрышей</p>
            </div>

            <?php foreach ($countNumbers as $key => $value) :?>
                <div class='wrapper-result'>
                    <div class='how'> <?=$key+1?>x<?=NUMBER_INPUT?> </div>
                    <p class='result-value'> <?=$value?> </p>
                </div>
            <?php endforeach;?>

        </div>
    </section>
    <?php
}


// function

function showErr($keyInput, $arrayErr)
{
    if (is_array($arrayErr)) {
        foreach ($arrayErr as $key => $value) {
            if (count($value) < NUMBER_INPUT) {
                if ($key == $keyInput) { // если имя инпута совпадает с ключем
                    foreach ($value as $item) { // выводи ошибку по ключу который получил
                        return $item;
                    }
                }
            }
        }
    }
}

function test_input($data)
{
    $err = [];
    $catchTest = [];

    foreach ($data as $key => $value) {
        if (empty($value)) {
            $err[$key][] = "ввод обязателен";
        }
        if (!in_array($value, $catchTest)) {  // если нету элемента то наполняй массив
            $catchTest[] = $value;  // наполняется пока in_array не увидит похожий (наполняется на каждой итерации)
        } else {
            $err[$key][] = 'без повторов'; // если увидел то ошибка
        }
        if (!is_numeric($value)) {
            $err[$key][] = 'введите число';
        }
        if ($value <= 0 || $value > MAX_NUM) {
            $err[$key][] = "введите число от 1 до " . MAX_NUM;
        }
    }
    if (empty($err)) { // если массив err пустой возвращай массив с значениями
        return [$data];
    } else {
        return $err; // если нет выводи массив с ощибками
    }
}

function checkingErrors($array)
{ // флаг для проверки , если есть ошибка то не выводил результат
    foreach ($array as $value) {
        if (count($value) < NUMBER_INPUT) {
            return false;
        } else {
            return true;
        }
    }
}

function prepareFormData($numberInput, $validArray, $arrayEntered)
{
    $preparedArray = [];
    if (empty($arrayEntered)) {
        for ($i = 0; $i < $numberInput; $i++) {

            $preparedArray[] = [
                "name" => $i,
                "value" => "",
                "error" => "",
            ];

        }
    } else {
        foreach ($arrayEntered as $key => $value) {
            $showError = showErr($key, $validArray);

            $preparedArray[] = [
                "name" => $key,
                "value" => $value,
                "error" => $showError,
            ];
        }
    }
    return $preparedArray;
}

function parseFile ($file,$parseArray){  // принимает файл выдает массив
    $delimiter = "\n";  // по переводу строк
    $content = trim( file_get_contents($file) );// в строку
    $encodedCont = mb_convert_encoding( $content, 'UTF-8', mb_detect_encoding( $content, ['cp1251','UTF-8'] ) ); // декодировка , код в строке

    $lines = explode( $delimiter, trim($encodedCont) ); // в массив
    $lines = array_slice($lines,1);// убрал заголовки

    $data = [];
    foreach( $lines as $key => $line ){
        $data[] = str_getcsv($line , ";"); // помещаю все в отдельные массивы
        unset( $lines[$key] );
    }

    foreach($data as $key => $line){
        $parseArray[] = array_slice($line,4,6); // обрезаю и оставляю 6 цифр с которыми буду работать
        unset($data[$key]);
    }
    return $parseArray;
}

function countWins ($parseArray,$count,$array){ // считает совпадения
    foreach ($parseArray as $value){
        $sumIntersect = count(array_intersect($value,$array)); // ищу похожих и их количество
        if ($sumIntersect >= 0){
            $count[$sumIntersect]++; // если есть совпадение по ключу sumIntersect полюсуй
            ksort($count);
        }
    }
    return $count;
}



